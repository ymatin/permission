package com.oburlin.ss.interceptors;

import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;

import java.sql.Statement;
import java.util.Properties;


//签名告诉插件要拦截哪个对象的哪个方法
@Intercepts({
        @Signature(type= ResultSetHandler.class,method = "handleResultSets",args = Statement.class)
})
public class FirstiBaitsInterceptors implements Interceptor {

    //拦截目标对象的目标方法
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
//        invocation.getTarget() ; //获取拦截对象
        System.out.println(invocation.getTarget());
        Object object=invocation.proceed();
        return object;
    }

    //为目标对象创建代理对象
    @Override
    public Object plugin(Object target) {
        System.out.println(target);
        return Interceptor.super.plugin(target);
    }

    //加载配置初始化
    @Override
    public void setProperties(Properties properties) {
        Interceptor.super.setProperties(properties);
        System.out.println(properties);
    }
}
