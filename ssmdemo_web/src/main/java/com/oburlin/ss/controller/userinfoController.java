package com.oburlin.ss.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.oburlin.ss.domain.userinfo;
import com.oburlin.ss.service.userinfoService;
import com.oburlin.ss.utis.pageUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/user")
public class userinfoController {

    @Autowired
    private userinfoService userinfoService;

    @RequestMapping("/userdetail")
    public  String finderDetail(Model model){

        userinfo  userinfo=userinfoService.findById(1);
        model.addAttribute("userinfo",userinfo);
        /*页面上的属性，返回页面*/
        return "userdetail";
    }

    @RequestMapping("/users")
    public  String finderDetail1(HttpServletRequest request, Model model)throws IOException {

        String num=request.getParameter("num");
        if(null==num)
        {
            num="1";
        }
        pageUtil page=null;
        try {
            page   = userinfoService.getAllUsers(num);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        model.addAttribute("page",page);
        /*页面上的属性，返回页面*/
        return "users";
    }
    @RequestMapping("/users2")
    public  String finderDetail2(HttpServletRequest request, Model model)throws IOException {

        String num=request.getParameter("num");
        if(null==num)
        {
            num="1";
        }

        Page<?> page = PageHelper.startPage(Integer.parseInt(num),5);
        List<userinfo> users  = userinfoService.getAllUsersByPageHelper();
        PageInfo<?> pageInfo =page.toPageInfo();

        model.addAttribute("users",users);
        model.addAttribute("pageinfo",pageInfo);
        /*页面上的属性，返回页面*/
        return "users2";
    }

}
