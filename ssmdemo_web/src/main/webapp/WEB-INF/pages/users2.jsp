<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <%--<base href="<%=basePath%>">--%>

    <title>My JSP 'users.jsp' starting page</title>


</head>

<body>

<table align="center" width="80%" border="1">
    <tr>
        <td>id</td>
        <td>username</td>
        <td>address</td>
        <td>password</td>
    </tr>

    <c:forEach items="${users}" var="user">

        <tr>
            <td>${user.id} </td>
            <td>${user.username}</td>
            <td>${user.email}</td>
            <td>${user.password}</td>
        </tr>
        <br>

    </c:forEach>
</table>


<div align="center">

    共${requestScope.pageinfo.total}条/共${requestScope.pageinfo.pages}页
    <a href="${pageContext.request.contextPath}/user/users2?num=1">首页</a>

    <a href="${pageContext.request.contextPath}/user/users2?num=${requestScope.pageinfo.prePage}">上一页</a>

    <c:forEach   items="${requestScope.pageinfo.navigatepageNums}" var="num">
        <a href="${pageContext.request.contextPath}/user/users2?num=${num}">${num}</a>
    </c:forEach>

    <a href="${pageContext.request.contextPath}/user/users2?num=${requestScope.pageinfo.nextPage}">下一页</a>
    <a href="${pageContext.request.contextPath}/user/users2?num=${requestScope.pageinfo.total}">末页</a>


    跳转到
    <input id="number" type="text" name="hello" size="6">页<input type="button" value="跳转"
                                                                 onclick="changeNumber()"></input>

    <script>
        function changeNumber() {
            var num = document.getElementById("number").value;

            //是否是数字  输入的数字一定是整数或者是小于总页数的值

            window.location.href = "${pageContext.request.contextPath}/user/users2?num=" + num;


        }


    </script>


</div>


</body>
</html>
