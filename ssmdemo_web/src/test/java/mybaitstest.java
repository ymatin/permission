import com.oburlin.ss.dao.userinfoDao;
import com.oburlin.ss.domain.userinfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.io.Reader;

public class mybaitstest {

    public class mybaitscoll {
        public static SqlSessionFactory sqlSessionFactory = null;

        public static SqlSessionFactory getSqlSessionFactory() {
            if (sqlSessionFactory == null) {
                String resource = "mybatis-config.xml";
                try {
                    Reader reader = Resources.getResourceAsReader(resource);
                    sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sqlSessionFactory;
        }
    }

    public static void main(String[] args) {

        SqlSession sqlSession = mybaitscoll.getSqlSessionFactory().openSession();

        userinfoDao personMapper = sqlSession.getMapper(userinfoDao.class);


        userinfo person = personMapper.findById(1);

        System.out.println(person);

    }
}
