package com.oburlin.ss.service;

import com.oburlin.ss.domain.userinfo;
import com.oburlin.ss.utis.pageUtil;

import java.util.List;


public interface userinfoService {

    public userinfo findById(int id);


    public pageUtil getAllUsers(String num) throws  Exception;

   public List<userinfo> getAllUsersByPageHelper();

}
