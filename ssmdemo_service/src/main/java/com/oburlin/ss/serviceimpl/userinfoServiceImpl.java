package com.oburlin.ss.serviceimpl;


import com.oburlin.ss.dao.userinfoDao;
import com.oburlin.ss.domain.userinfo;
import com.oburlin.ss.service.userinfoService;
import com.oburlin.ss.utis.pageUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

@Service
public class userinfoServiceImpl implements userinfoService {

    @Autowired
    private userinfoDao userinfoDao ;


    @Override

    public userinfo findById(int id) {
        return userinfoDao.findById(id);
    }


    @Override
    public pageUtil getAllUsers(String num) throws Exception {

        int currentPageNum=1;

        if(null!=num&&!num.trim().equals(""))
        {
            currentPageNum=Integer.parseInt(num);
        }

        int totalRecords=userinfoDao.totalRecords();

        pageUtil pu=new pageUtil(currentPageNum,totalRecords);

        List<userinfo> users= userinfoDao.getAllUsers(pu.getStartIndex(),pu.getPageSize());

        pu.setRecords(users);

        return pu;
    }

    @Override
    public List<userinfo> getAllUsersByPageHelper() {

        SqlSession sqlSession = mybaitscoll.getSqlSessionFactory().openSession();
        userinfoDao userMapper = sqlSession.getMapper(userinfoDao.class);
        List<userinfo> users= userMapper.getAllUsersByPageHelper();
        return users;
    }


    public class mybaitscoll {
        public static SqlSessionFactory sqlSessionFactory = null;

        public static SqlSessionFactory getSqlSessionFactory() {
            if (sqlSessionFactory == null) {
                String resource = "mybatis-config.xml";
                try {
                    Reader reader = Resources.getResourceAsReader(resource);
                    sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sqlSessionFactory;
        }
    }
}
