package com.oburlin.ss.dao;

import com.oburlin.ss.domain.userinfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface userinfoDao {

    public userinfo findById(int id);

    List<userinfo> getAllUsers(@Param("startIndex")int startIndex ,@Param("pagesize")int pagesize) throws  Exception;//limit ?,?

    int totalRecords() throws  Exception;

    List<userinfo> getAllUsersByPageHelper();

}
