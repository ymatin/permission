package com.oburlin.ss.test;

import com.oburlin.ss.dao.userinfoDao;
import com.oburlin.ss.domain.userinfo;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class userTest {


    @Test
    public  void  findById(){
        ApplicationContext applicationContext =  new ClassPathXmlApplicationContext("applicationContext.xml");

        userinfoDao  userinfoDao = applicationContext.getBean(userinfoDao.class);

        userinfo user=userinfoDao.findById(1);

        System.out.println(user);

    }
}
